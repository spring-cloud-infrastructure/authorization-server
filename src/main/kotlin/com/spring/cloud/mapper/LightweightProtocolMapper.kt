package com.spring.cloud.mapper

import org.keycloak.models.ClientSessionContext
import org.keycloak.models.KeycloakSession
import org.keycloak.models.ProtocolMapperModel
import org.keycloak.models.UserSessionModel
import org.keycloak.protocol.ProtocolMapperUtils
import org.keycloak.protocol.oidc.mappers.AbstractOIDCProtocolMapper
import org.keycloak.protocol.oidc.mappers.OIDCAccessTokenMapper
import org.keycloak.protocol.oidc.mappers.OIDCAttributeMapperHelper.addIncludeInTokensConfig
import org.keycloak.protocol.oidc.mappers.OIDCAttributeMapperHelper.createClaimMapper
import org.keycloak.protocol.oidc.mappers.OIDCAttributeMapperHelper.includeInAccessToken
import org.keycloak.protocol.oidc.mappers.OIDCIDTokenMapper
import org.keycloak.protocol.oidc.mappers.UserInfoTokenMapper
import org.keycloak.provider.ProviderConfigProperty
import org.keycloak.provider.ProviderConfigProperty.BOOLEAN_TYPE
import org.keycloak.provider.ProviderConfigurationBuilder
import org.keycloak.representations.AccessToken
import java.lang.Boolean.parseBoolean

class LightweightProtocolMapper : AbstractOIDCProtocolMapper(),
    OIDCAccessTokenMapper,
    OIDCIDTokenMapper,
    UserInfoTokenMapper {

    companion object {
        private const val PROVIDER_ID = "oidc-lightweight-jwt"
        private const val SCOPE_CLAIM = "Scope"
        private const val ISSUED_FOR_CLAIM = "Issued For"
        private const val SESSION_STATE_CLAIM = "Session State"
        private const val AUTHENTICATION_CONTEXT_CLAIM = "Authentication Context"
        private const val RESOURCE_ACCESS_CLAIM = "Resource Access"
        private const val EMAIL_CLAIM = "Email"
        private const val EMAIL_VERIFIED_CLAIM = "Email Verified"
        private const val PREFERRED_USERNAME_CLAIM = "Preferred Username"
        private const val REALM_ACCESS_CLAIM = "Realm Access"
        private const val TOKEN_TYPE_CLAIM = "Token Type"
        private const val TOKEN_ISSUER_CLAIM = "Token Issuer"
        private const val OTHER_CLAIMS_CLAIM = "Other Claims"
        private const val ALLOWED_ORIGINS_CLAIM = "Allowed Origins"
        private const val TOKEN_AUDIENCE_CLAIM = "Token Audience"
        private const val TOKEN_ID_CLAIM = "Token Id"
        private const val TOKEN_ISSUED_AT_CLAIM = "Token Issued At"

        private fun buildProviderConfigProperty(
            claimName: String,
            isEnabledByDefault: Boolean,
        ): ProviderConfigProperty = ProviderConfigProperty().apply {
            name = claimName
            label = "$claimName claim"
            helpText = "Should the $claimName claim be included to the token?"
            type = BOOLEAN_TYPE
            defaultValue = isEnabledByDefault
        }

        private val CONFIG_PROPERTIES: List<ProviderConfigProperty> = ProviderConfigurationBuilder.create()
            .apply {
                property(buildProviderConfigProperty(SCOPE_CLAIM, false))
                property(buildProviderConfigProperty(ISSUED_FOR_CLAIM, true))
                property(buildProviderConfigProperty(SESSION_STATE_CLAIM, true))
                property(buildProviderConfigProperty(AUTHENTICATION_CONTEXT_CLAIM, false))
                property(buildProviderConfigProperty(RESOURCE_ACCESS_CLAIM, false))
                property(buildProviderConfigProperty(EMAIL_CLAIM, false))
                property(buildProviderConfigProperty(EMAIL_VERIFIED_CLAIM, false))
                property(buildProviderConfigProperty(PREFERRED_USERNAME_CLAIM, false))
                property(buildProviderConfigProperty(REALM_ACCESS_CLAIM, false))
                property(buildProviderConfigProperty(TOKEN_TYPE_CLAIM, true))
                property(buildProviderConfigProperty(TOKEN_ISSUER_CLAIM, true))
                property(buildProviderConfigProperty(OTHER_CLAIMS_CLAIM, false))
                property(buildProviderConfigProperty(ALLOWED_ORIGINS_CLAIM, false))
                property(buildProviderConfigProperty(TOKEN_AUDIENCE_CLAIM, false))
                property(buildProviderConfigProperty(TOKEN_ID_CLAIM, false))

            }.build().apply {
                addIncludeInTokensConfig(this, LightweightProtocolMapper::class.java)
            }

        fun create(
            name: String?,
            userAttribute: String?,
            tokenClaimName: String?,
            claimType: String?,
            accessToken: Boolean,
            idToken: Boolean,
            isScopeEnabled: Boolean,
            isIssuedForEnabled: Boolean,
            isSessionStateEnabled: Boolean,
            isAuthenticationContextEnabled: Boolean,
            isResourceAccessEnabled: Boolean,
            isEmailEnabled: Boolean,
            isEmailVerifiedEnabled: Boolean,
            isPreferredUsernameEnabled: Boolean,
            isRealmAccessEnabled: Boolean,
            isTokenTypeEnabled: Boolean,
            isTokenIssuerEnabled: Boolean,
            isOtherClaimsEnabled: Boolean,
            isAllowedOriginsEnabled: Boolean,
            isTokenAudienceEnabled: Boolean,
            isTokenIdEnabled: Boolean,
        ): ProtocolMapperModel = createClaimMapper(
            name,
            userAttribute,
            tokenClaimName,
            claimType,
            accessToken,
            idToken,
            PROVIDER_ID
        ).apply {
            config[SCOPE_CLAIM] = "$isScopeEnabled"
            config[ISSUED_FOR_CLAIM] = "$isIssuedForEnabled"
            config[SESSION_STATE_CLAIM] = "$isSessionStateEnabled"
            config[AUTHENTICATION_CONTEXT_CLAIM] = "$isAuthenticationContextEnabled"
            config[RESOURCE_ACCESS_CLAIM] = "$isResourceAccessEnabled"
            config[EMAIL_CLAIM] = "$isEmailEnabled"
            config[EMAIL_VERIFIED_CLAIM] = "$isEmailVerifiedEnabled"
            config[PREFERRED_USERNAME_CLAIM] = "$isPreferredUsernameEnabled"
            config[REALM_ACCESS_CLAIM] = "$isRealmAccessEnabled"
            config[TOKEN_TYPE_CLAIM] = "$isTokenTypeEnabled"
            config[TOKEN_ISSUER_CLAIM] = "$isTokenIssuerEnabled"
            config[OTHER_CLAIMS_CLAIM] = "$isOtherClaimsEnabled"
            config[ALLOWED_ORIGINS_CLAIM] = "$isAllowedOriginsEnabled"
            config[TOKEN_AUDIENCE_CLAIM] = "$isTokenAudienceEnabled"
            config[TOKEN_ID_CLAIM] = "$isTokenIdEnabled"
        }
    }

    override fun getConfigProperties(): List<ProviderConfigProperty> = CONFIG_PROPERTIES

    override fun getId(): String = PROVIDER_ID

    override fun getDisplayType(): String = "Lightweight JWT Protocol Mapper"

    override fun getDisplayCategory(): String = TOKEN_MAPPER_CATEGORY

    override fun getHelpText(): String = "Lightweight JWT Protocol Mapper"

    override fun getPriority(): Int = ProtocolMapperUtils.PRIORITY_SCRIPT_MAPPER

    // TODO: implement methods for transformation ID token as well as user info token

    override fun transformAccessToken(
        token: AccessToken,
        mappingModel: ProtocolMapperModel,
        session: KeycloakSession,
        userSession: UserSessionModel,
        clientSession: ClientSessionContext,

        ): AccessToken {
        if (!includeInAccessToken(mappingModel)) {
            return token
        }
        if (isClaimDisabled(mappingModel, SCOPE_CLAIM)) {
            token.scope = null
        }
        if (isClaimDisabled(mappingModel, ISSUED_FOR_CLAIM)) {
            token.issuedFor(null)
        }
        if (isClaimDisabled(mappingModel, SESSION_STATE_CLAIM)) {
            token.sessionState = null
        }
        if (isClaimDisabled(mappingModel, AUTHENTICATION_CONTEXT_CLAIM)) {
            token.acr = null
        }
        if (isClaimDisabled(mappingModel, RESOURCE_ACCESS_CLAIM)) {
            token.resourceAccess = null
        }
        if (isClaimDisabled(mappingModel, EMAIL_CLAIM)) {
            token.email = null
        }
        if (isClaimDisabled(mappingModel, EMAIL_VERIFIED_CLAIM)) {
            token.emailVerified = null
        }
        if (isClaimDisabled(mappingModel, PREFERRED_USERNAME_CLAIM)) {
            token.preferredUsername = null
        }
        if (isClaimDisabled(mappingModel, REALM_ACCESS_CLAIM)) {
            token.realmAccess = null
        }
        if (isClaimDisabled(mappingModel, TOKEN_TYPE_CLAIM)) {
            token.type(null)
        }
        if (isClaimDisabled(mappingModel, TOKEN_ISSUER_CLAIM)) {
            token.issuer(null)
        }
        if (isClaimDisabled(mappingModel, OTHER_CLAIMS_CLAIM)) {
            token.otherClaims.clear()
        }
        if (isClaimDisabled(mappingModel, ALLOWED_ORIGINS_CLAIM)) {
            token.allowedOrigins = null
        }
        if (isClaimDisabled(mappingModel, TOKEN_AUDIENCE_CLAIM)) {
            token.audience(null)
        }
        if (isClaimDisabled(mappingModel, TOKEN_ID_CLAIM)) {
            token.id(null)
        }
        if (isClaimDisabled(mappingModel, TOKEN_ISSUED_AT_CLAIM)) {
            token.iat(null)
        }
        return token
    }

    private fun isClaimEnabled(mappingModel: ProtocolMapperModel, claimName: String): Boolean =
        mappingModel.let(ProtocolMapperModel::getConfig)
            .let { it[claimName] }
            .let(::parseBoolean)

    private fun isClaimDisabled(mappingModel: ProtocolMapperModel, claimName: String): Boolean =
        !isClaimEnabled(mappingModel, claimName)
}
