package com.spring.cloud.config

import com.github.thomasdarimont.keycloak.embedded.EmbeddedKeycloakApplication
import com.github.thomasdarimont.keycloak.embedded.KeycloakCustomProperties
import com.github.thomasdarimont.keycloak.embedded.KeycloakCustomProperties.Migration
import mu.KotlinLogging.logger
import org.apache.commons.io.IOUtils.copy
import org.springframework.core.io.FileSystemResource
import org.springframework.core.io.Resource
import org.springframework.core.io.UrlResource
import org.springframework.web.context.support.WebApplicationContextUtils.getRequiredWebApplicationContext
import java.io.File.createTempFile
import java.io.FileOutputStream
import java.io.IOException
import javax.servlet.ServletContext
import javax.ws.rs.core.Context

class CustomEmbeddedKeycloakApplication(@Context context: ServletContext) : EmbeddedKeycloakApplication(context) {

    private val logger = logger { }

    companion object {
        private const val KEYCLOAK_REALM_CONFIG_FILE_NAME = "keycloak-realm-config"
        private const val JSON_FILE_POSTFIX = ".json"
        private const val SINGLE_FILE_IMPORT_PROVIDER = "singleFile"
    }

    private val customProperties: KeycloakCustomProperties = getRequiredWebApplicationContext(context).getBean(
        KeycloakCustomProperties::class.java
    )

    override fun tryImportRealm() {
        if (isMigrationConfigFileUrlResource()) {
            customProperties.migration.importLocation = downloadMigrationConfigFile()
        }
        super.tryImportRealm()
    }

    private fun downloadMigrationConfigFile(): Resource {
        val migrationConfigFile = customProperties.migration.importLocation
        return try {
            val tempFile = createTempFile(KEYCLOAK_REALM_CONFIG_FILE_NAME, JSON_FILE_POSTFIX).apply {
                deleteOnExit()
            }
            FileOutputStream(tempFile).use { copy(migrationConfigFile.inputStream, it) }
            FileSystemResource(tempFile)

        } catch (e: IOException) {
            logger.warn(e) { "Migration config file cannot be downloaded from [$migrationConfigFile], trying to fallback to default migration config file location" }
            Migration().importLocation
        }
    }

    private fun isMigrationConfigFileUrlResource(): Boolean {
        val migration = customProperties.migration
        return (SINGLE_FILE_IMPORT_PROVIDER == migration.importProvider
                && migration.importLocation.exists()
                && migration.importLocation is UrlResource)
    }
}
