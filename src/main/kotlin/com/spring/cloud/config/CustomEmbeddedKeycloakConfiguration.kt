package com.spring.cloud.config

import com.github.thomasdarimont.keycloak.embedded.EmbeddedKeycloakConfig
import com.github.thomasdarimont.keycloak.embedded.KeycloakCustomProperties
import org.jboss.resteasy.plugins.server.servlet.HttpServlet30Dispatcher
import org.springframework.boot.web.servlet.ServletRegistrationBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
internal class CustomEmbeddedKeycloakConfiguration : EmbeddedKeycloakConfig() {

    @Bean
    override fun keycloakJaxRsApplication(customProperties: KeycloakCustomProperties): ServletRegistrationBean<HttpServlet30Dispatcher> =
        super.keycloakJaxRsApplication(customProperties).apply {
            addInitParameter("javax.ws.rs.Application", CustomEmbeddedKeycloakApplication::class.java.name)
        }
}
