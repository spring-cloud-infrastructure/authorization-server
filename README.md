# Authorization Server

Check out [documentation](https://github.com/thomasdarimont/embedded-spring-boot-keycloak-server) for more info about
embedded Keycloak Authorization Server.

## 1. Plugins

- [keycloak-event-listener-rabbitmq](https://github.com/aznamier/keycloak-event-listener-rabbitmq) - Keycloak SPI plugin
  that publishes events to a RabbitMq server.

See [Keycloak extensions](https://www.keycloak.org/extensions.html) for more available plugins.

## 2. Configuration

- [bootstrap.yml](./src/main/resources/bootstrap.yml) (application bootstrap only)
- [config-repo](https://gitlab.com/spring-cloud-infrastructure/config-repo/-/tree/master/config) (externalized
  configuration)

### Profiles

- `h2` - Use H2 in-memory DB.
- `postgres` - Use Postgres DB.

## 3. Build & Run

Check out [docker](https://gitlab.com/spring-cloud-infrastructure/docker) project.
